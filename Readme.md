# Rules

- Enjoy
- TDD approach

# Bibliografia

- Generated from https://maven.apache.org/archetypes/maven-archetype-quickstart/
- [Junit5](https://www.baeldung.com/junit-5)

# Execute

This project can be run either using docker or a local maven installation. This project assumes the reader knows how to
set them up.

## With docker

In order to run this project using docker it is required to have a docker installation running.

#### Running the tests

```shell
docker run --rm -it -w /code -v $(pwd):/code maven:latest mvn clean test
```

> **NOTICE** that this README assumes the user's OS is linux based. In case you are using windows we encourage using 
> windows linux subsystem. Otherwise, this command will not work

## With local maven installation

This project is set up to work with Java 11, so in order to run this project using a local installation a JDK 11
**MUST** be installed as well as maven.

#### Running the tests

```shell
mvn clean test
```
